# spring-boot-scaffolding

## 介绍
Spring Boot 脚手架，用于快速搭建项目框架。

## 软件架构
使用 FreeMarker 作为模板引擎，先生成 Spring Boot 代码，再借助 mybatis-plus-generator 生成 DAO 层代码。

## 使用说明
1. 在 application.yml 中配置项目信息；

   ![image-20230228173815520](README.assets/image-20230228173815520.png)

2. 执行 com.carp.scaffolding.SpringBootScaffoldingApplicationTests#test 方法，自动生成项目代码至 output 目录下。

   ![image-20230228162714470](README.assets/image-20230228162714470.png)