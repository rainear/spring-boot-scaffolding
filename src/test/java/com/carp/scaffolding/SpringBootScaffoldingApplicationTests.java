package com.carp.scaffolding;

import com.carp.scaffolding.service.IGenerateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SpringBootScaffoldingApplicationTests {

    @Autowired
    private IGenerateService generateServiceImpl;

    @Test
    public void test() {
        generateServiceImpl.generate();
    }

}
