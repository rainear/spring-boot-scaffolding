<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <artifactId>${artifactId}</artifactId>
    <version>1.0</version>
    <packaging>jar</packaging>

    <name><#noparse>${project.artifactId}</#noparse></name>
    <description>${description}</description>

    <!-- 使用ysb-spring-boot作为parent，简化各种配置 -->
    <parent>
        <groupId>com.ysb</groupId>
        <artifactId>ysb-spring-boot-parent</artifactId>
        <version>${ysbVersion}</version>
    </parent>

    <dependencies>

        <!-- ysb starters -->
        <dependency>
            <groupId>com.ysb</groupId>
            <artifactId>ysb-spring-boot-web-base-starter</artifactId>
        </dependency>

        <dependency>
            <groupId>com.ysb</groupId>
            <artifactId>ysb-spring-boot-user-starter</artifactId>
        </dependency>

        <dependency>
            <groupId>com.ysb</groupId>
            <artifactId>ysb-spring-boot-email-starter</artifactId>
        </dependency>

        <!-- service monitor -->
        <dependency>
            <groupId>com.ysb</groupId>
            <artifactId>ysb-spring-boot-admin-client-starter</artifactId>
        </dependency>

        <!-- spring boot starters -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.3.2</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>

        <#if rabbitMQ>
        <!-- rabbitmq dependency  -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-amqp</artifactId>
        </dependency>
        </#if>

        <#if redisson>
        <!-- https://mvnrepository.com/artifact/org.redisson/redisson-spring-boot-starter -->
        <dependency>
            <groupId>org.redisson</groupId>
            <artifactId>redisson-spring-boot-starter</artifactId>
            <version>3.14.1</version>
        </dependency>
        </#if>

        <!-- error log output-->
        <dependency>
            <groupId>org.apache.kafka</groupId>
            <artifactId>kafka-clients</artifactId>
        </dependency>

        <!-- alibaba fastjson for fosun creditpay -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.38</version>
        </dependency>

    </dependencies>

    <!-- 使用Maven私服： -->
    <repositories>
        <repository>
            <id>nexus</id>
            <name>Nexus Repository</name>
            <url>http://maven.ysbang.cn:8081/repository/maven-public/</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>nexus</id>
            <name>Nexus Plugin Repository</name>
            <url>http://maven.ysbang.cn:8081/repository/maven-public/</url>
        </pluginRepository>
    </pluginRepositories>

</project>
