package com.ysb.config.rabbit.producer;

import com.ysb.config.rabbit.AbstractRabbitConfig;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.ysb.constant.RabbitConstants.HIGHEST_SEAL_PRIORITY;
import static com.ysb.constant.RabbitConstants.SEAL_QUEUE_NAME;

/**
 * @author scaffolding
 * @since ${dateTime}
 */
@Configuration
public class RabbitProducerConfig extends AbstractRabbitConfig {

    @Bean
    public Queue sealQueue() {
        return QueueBuilder
                .durable(SEAL_QUEUE_NAME)
                // Each priority level uses an internal queue on the Erlang VM, which takes up some resources. In most use cases it is sufficient to have no more than 5 priority levels.
                .maxPriority(HIGHEST_SEAL_PRIORITY)
                //  If you are sending a lot of messages at once (e.g. processing batch jobs) or if you think that your consumers will not keep up with the speed of the publishers all the time, we recommend you to enable lazy queues.
                .lazy()
                .build();
    }

    @Bean
    public Binding sealBinding(DirectExchange drugQualityExchange, Queue sealQueue) {
        return BindingBuilder.bind(sealQueue).to(drugQualityExchange).with(SEAL_QUEUE_NAME);
    }

}
