package com.ysb;

<#if rabbitMQ>
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
</#if>
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author scaffolding
 * @since ${dateTime}
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableAsync
<#if rabbitMQ>
@EnableRabbit
</#if>
public class ${applicationName} {
    public static void main(String[] args) {
        SpringApplication.run(${applicationName}.class, args);
    }
}


