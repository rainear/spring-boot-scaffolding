package com.ysb.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author scaffolding
 * @since ${dateTime}
 */
@Configuration
public class RedissonConfig {
    @Bean
    public RedissonClient redissonClient(RedisProperties redisProperties) {
        List<String> nodes = redisProperties.getCluster().getNodes();
        String[] nodeAddresses = nodes.stream().map(node -> "redis://" + node).toArray(String[]::new);
        Config config = new Config();
        config.setNettyThreads(10);
        config.useClusterServers()
                .addNodeAddress(nodeAddresses)
                .setConnectTimeout((int) redisProperties.getTimeout().toMillis());
        return Redisson.create(config);
    }
}