package com.ysb.constant;

/**
 * @author scaffolding
 * @since ${dateTime}
 */
public class RabbitConstants {

    public static final String LISTENER_CONTAINER_FACTORY_NAME = "simpleRabbitListenerContainerFactory";
    public static final String DRUG_QUALITY_EXCHANGE_NAME = "drug.quality.exchange";
    public static final String SEAL_QUEUE_NAME = "seal.queue";
    public static final String SPRING_RETURNED_MESSAGE_CORRELATION = "spring_returned_message_correlation";
    public static final int MIN_CONSUMERS = 20;
    public static final int MAX_CONSUMERS = 30;
    public static final int HIGHEST_SEAL_PRIORITY = 10;

}
