ysb:
  boot:
    cache:
      defaultMaxSize: 65535
      defaultTtlSeconds: 10
      caches:
        # 字典表缓存：
        - name: getValueAndNamesByClsCode
          maxSize: 1000
          ttlSeconds: 600 #10分钟
        - name: getValueAndNamesByClsCodeAll
          maxSize: 100
          ttlSeconds: 600 #10分钟
        # 系统配置缓存：
        - name: getSysConfigValuesByItem
          maxSize: 1000
          ttlSeconds: 300 #5分钟
        # 地区相关缓存：
        - name: getAreaById
          maxSize: 5000
          ttlSeconds: 900 #15分钟
        - name: getIdByAreaCode
          maxSize: 1000
          ttlSeconds: 900 #15分钟
        - name: getChildrenByAreaId
          maxSize: 1000
          ttlSeconds: 900 #15分钟
        - name: getIdByProvinceAndCityNames
          maxSize: 1000
          ttlSeconds: 900 #15分钟
        # 其他……：