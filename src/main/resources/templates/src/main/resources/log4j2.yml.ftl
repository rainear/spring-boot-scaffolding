Configuration:
  status: warn

  Properties: 
    Property: # 缺省配置（用于开发环境）。其他环境需要在VM参数中指定，如下：
      #测试：-Dlog.level.console=warn -Dlog.level.ysb=trace
      #生产：-Dlog.level.console=warn -Dlog.level.ysb=info      
      - name: log.level.console
        value: trace
      - name: log.level.ysb
        value: trace
      - name: log.path
        value: /opt/logs
      - name: kafka.servers
        value: 192.168.0.31:9092
      - name: project.name
        value: ${name}

<#noparse>
  Appenders:
    Console:  #输出到控制台
      name: CONSOLE
      target: SYSTEM_OUT
      ThresholdFilter:
        level: ${sys:log.level.console}
        onMatch: ACCEPT
        onMismatch: DENY
      PatternLayout:
        pattern: "%highlight{%d{yyyy-MM-dd HH:mm:ss,SSS}:%4p [%X{X-B3-TraceId}] %t (%F:%L) - %m%n}{STYLE=Logback}"
        disableAnsi: false
    RollingRandomAccessFile: # 输出到文件，超过128MB分割
      - name: ROLLING_RANDOM_ACCESS_FILE
        ignoreExceptions: false
        fileName: ${log.path}/${project.name}.log
        filePattern: "${log.path}/$${date:yyyy-MM}/${project.name}-%d{yyyy-MM-dd}-%i.log.gz"
        PatternLayout:
          pattern: "%d{yyyy-MM-dd HH:mm:ss,SSS}:%4p [%X{X-B3-TraceId}] %t (%F:%L) - %m%n"
        Policies:
          SizeBasedTriggeringPolicy:
            size: "128 MB"
          TimeBasedTriggeringPolicy :
            interval: 1
        DefaultRolloverStrategy:
          max: 1000
      - name: ECS_ROLLING_RANDOM_ACCESS_FILE
        ignoreExceptions: false
        fileName: ${log.path}/${project.name}.log.json
        filePattern: "${log.path}/temp/${project.name}-%i.log.json"
        EcsLayout:
          serviceName: ${project.name}
        Policies:
          SizeBasedTriggeringPolicy:
            size: "512 MB"
        DefaultRolloverStrategy:
          max: 10


    Kafka: #输出错误日志到Kafka
      name: KAFKA
      topic: ysb-error-logs
      syncSend: false
      ThresholdFilter:
        level: error
        onMatch: ACCEPT
        onMismatch: DENY
      PatternLayout:
        pattern: "${sys:svc-name} ${sys:local-ip} %d{yyyy-MM-dd HH:mm:ss,SSS}:%4p [%X{X-B3-TraceId}] %t (%F:%L) - %m%n"
      Property:
        - name: bootstrap.servers
          value: ${sys:kafka.servers}

  Loggers:
    Root:
      level: info
      AppenderRef:
        - ref: CONSOLE
        - ref: ROLLING_RANDOM_ACCESS_FILE
        - ref: ECS_ROLLING_RANDOM_ACCESS_FILE
#        - ref: KAFKA
    Logger: # 为com.ysb包配置特殊的Log级别，方便调试
      - name: com.ysb
        additivity: false
        level: ${sys:log.level.ysb}
        AppenderRef:
          - ref: CONSOLE
          - ref: ROLLING_RANDOM_ACCESS_FILE
          - ref: ECS_ROLLING_RANDOM_ACCESS_FILE
          - ref: KAFKA
</#noparse>