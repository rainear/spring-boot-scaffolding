ysb:
  boot:
    smsClient: # rabbitMQ 相关：
      rabbitmqUserName: ysb
      rabbitmqPassword: rabbitYsb
    swagger2: # 下面配置都可以不用，需要时可以配一下env
      env: dev, test, prod #控制哪个profile启用
      enabled: true
      base-package: com.ysb
      api-info:
        version: 2.0
        title: ${description} Api文档
        description: 这里是 ${description} 接口文档。
        contact:
          name: XuJijun
          url: http://www.ysbang.cn
          email: xujijun@ysbang.cn
    logger:
      msLog4j:
        logMethods: # 需要记录日志的方法名列表
          - all     # all表示记录所有方法
        logExcludedMethods: # 不需要记录日志的方法名列表（优先级比需要记录的高）
          - excludedMethod