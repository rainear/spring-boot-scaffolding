spring:
  profiles:
    active: dev
  application:
    name: ${name}

server:
  port: ${serverPort}