package com.ysb;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author scaffolding
 * @since ${dateTime}
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class ${applicationName}Tests {

    @Test
    public void test() {

    }

}
