FROM openjdk:8
COPY target/${name}.jar ${name}.jar

EXPOSE ${serverPort} ${managePort}

ENV JAVA_ARGS=""
ENV APP_OPTS=""

CMD java -jar $JAVA_ARGS ${name}.jar $APP_OPTS