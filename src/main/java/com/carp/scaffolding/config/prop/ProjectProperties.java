package com.carp.scaffolding.config.prop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

/**
 * @author lile
 * @date 2023/2/16
 */
@Configuration
@ConfigurationProperties(prefix = "project")
@Data
public class ProjectProperties {

    public ProjectProperties() {
        this.dateTime = LocalDate.now().toString();
    }

    private String groupId;
    private String artifactId;
    private String version;
    private String name;
    private String description;
    private String ysbVersion;
    private String serverPort;
    private String managePort;
    private String applicationName;
    private String dateTime;
    private Boolean rabbitMQ;
    private Boolean redisson;

    public void setName(String name) {
        this.name = name;
        String[] split = name.split("-");
        StringBuilder sb = new StringBuilder();
        for (String word : split) {
            sb.append(word.substring(0, 1).toUpperCase()).append(word.substring(1));
        }
        sb.append("Application");
        this.applicationName = sb.toString();
    }

}
