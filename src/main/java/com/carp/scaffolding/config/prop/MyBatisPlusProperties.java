package com.carp.scaffolding.config.prop;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author lile
 * @date 2023/2/16
 */
@Configuration
@ConfigurationProperties(prefix = "mybatis-plus")
@ConditionalOnProperty(prefix = "mybatis-plus", name = "enable", havingValue = "true")
@Data
public class MyBatisPlusProperties {

    private Boolean enable;
    private String database;
    private String tables;
    private String tablePrefix;

}
