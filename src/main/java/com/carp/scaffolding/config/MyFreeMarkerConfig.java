package com.carp.scaffolding.config;

import com.carp.scaffolding.container.TemplateContainer;
import lombok.Getter;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * @author lile
 * @date 2023/2/22
 */
@Configuration
@Getter
public class MyFreeMarkerConfig {

    private final freemarker.template.Configuration configuration;

    public MyFreeMarkerConfig() {
        configuration = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_28);
        configuration.setDefaultEncoding("UTF-8");
    }

    @PostConstruct
    public void init() throws IOException {
        configuration.setDirectoryForTemplateLoading(TemplateContainer.TEMPLATE_DIR);
    }

}
