package com.carp.scaffolding.util;

import com.carp.scaffolding.checker.Checker;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author lile
 * @date 2023/2/22
 */
@Component
@RequiredArgsConstructor
public class CheckUtils {
    private final List<Checker> checkers;
    private final Map<File, Boolean> cache = Maps.newHashMap();

    public boolean checkAll(File file) {
        if (cache.containsKey(file)) {
            return cache.get(file);
        }
        boolean isPassed = true;
        for (Checker checker : checkers) {
            if (!checker.check(file)) {
                isPassed = false;
                break;
            }
        }
        cache.putIfAbsent(file, isPassed);
        return isPassed;
    }

}
