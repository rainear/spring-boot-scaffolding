package com.carp.scaffolding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootScaffoldingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootScaffoldingApplication.class, args);
    }

}
