package com.carp.scaffolding.checker;

import java.io.File;

/**
 * @author lile
 * @date 2023/2/22
 */
public abstract class Checker {

    public abstract boolean check(File file);

    protected String getFileName(File file) {
        String path = file.getPath();
        return path.substring(path.lastIndexOf("\\") + 1);
    }

}
