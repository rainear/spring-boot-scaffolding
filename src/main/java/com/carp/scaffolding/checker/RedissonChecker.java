package com.carp.scaffolding.checker;

import com.google.common.collect.ImmutableSet;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Set;

/**
 * @author lile
 * @date 2023/2/22
 */
@Component
@ConditionalOnProperty(prefix = "project", name = "redisson", havingValue = "false")
public class RedissonChecker extends Checker {

    private final Set<String> templates = ImmutableSet.of("RedissonConfig.java.ftl");

    @Override
    public boolean check(File file) {
        return !templates.contains(getFileName(file));
    }

}
