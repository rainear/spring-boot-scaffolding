package com.carp.scaffolding.checker;

import com.google.common.collect.ImmutableSet;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Set;

/**
 * @author lile
 * @date 2023/2/22
 */
@Component
@ConditionalOnProperty(prefix = "project", name = "rabbitMQ", havingValue = "false")
public class RabbitMqChecker extends Checker {

    private final Set<String> templates = ImmutableSet.of(
            "AbstractRabbitConfig.java.ftl",
            "RabbitConsumerConfig.java.ftl",
            "RabbitProducerConfig.java.ftl",
            "RabbitConstants.java.ftl");

    @Override
    public boolean check(File file) {
        return !templates.contains(getFileName(file));
    }

}
