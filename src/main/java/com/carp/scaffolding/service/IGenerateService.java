package com.carp.scaffolding.service;

/**
 * @author lile
 * @date 2023/2/16
 */
public interface IGenerateService {

    void generate();
}
