package com.carp.scaffolding.service.impl;

import com.carp.scaffolding.chain.ProcessorChain;
import com.carp.scaffolding.chain.ProcessorChainBuilder;
import com.carp.scaffolding.service.IGenerateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author lile
 * @date 2023/2/16
 */
@Service
@RequiredArgsConstructor
public class GenerateServiceImpl implements IGenerateService {
    private final ProcessorChainBuilder processorChainBuilder;

    @Override
    public void generate() {
        ProcessorChain chain = processorChainBuilder.build();
        chain.process();
    }

}
