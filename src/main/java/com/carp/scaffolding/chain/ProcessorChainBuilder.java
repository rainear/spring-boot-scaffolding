package com.carp.scaffolding.chain;

import com.carp.scaffolding.chain.processor.MyBatisPlusProcessor;
import com.carp.scaffolding.chain.processor.SpringBootProcessor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author lile
 * @since 2023/2/28
 */
@Component
@RequiredArgsConstructor
public class ProcessorChainBuilder {
    private final SpringBootProcessor springBootProcessor;
    @Autowired(required = false)
    private MyBatisPlusProcessor myBatisPlusProcessor;

    public ProcessorChain build() {
        ProcessorChain chain = new ProcessorChain();
        return chain.appendNext(springBootProcessor).appendNext(myBatisPlusProcessor);
    }

}
