package com.carp.scaffolding.chain;

import com.carp.scaffolding.chain.processor.Processor;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * @author lile
 * @since 2023/2/27
 */
@Slf4j
public class ProcessorChain extends Processor {

    private final Processor first = new Processor() {
        @Override
        public void process() {
            processNext();
        }
    };

    private Processor end = first;

    @Override
    public void process() {
        first.process();
    }

    public ProcessorChain appendNext(Processor processor) {
        if (Objects.isNull(processor)) {
            return this;
        }
        end.setNext(processor);
        end = processor;
        return this;
    }

}


