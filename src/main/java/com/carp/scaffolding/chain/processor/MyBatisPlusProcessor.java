package com.carp.scaffolding.chain.processor;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.carp.scaffolding.config.prop.MyBatisPlusProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

/**
 * @author lile
 * @since 2023/2/28
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnBean(MyBatisPlusProperties.class)
public class MyBatisPlusProcessor extends Processor {
    private final AutoGenerator myBatisPlusGenerator;

    @Override
    public void process() {
        log.info("开始生成 DAO 层相关代码...");
        myBatisPlusGenerator.execute(new FreemarkerTemplateEngine());
        log.info("DAO 层相关代码生成完毕");
        processNext();
    }
}
