package com.carp.scaffolding.chain.processor;

import lombok.Setter;

import java.util.Objects;

/**
 * @author lile
 * @since 2023/2/27
 */
@Setter
public abstract class Processor {

    private Processor next = null;

    public abstract void process();

    public void processNext() {
        if (Objects.nonNull(next)) {
            next.process();
        }
    }

}
